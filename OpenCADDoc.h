
// OpenCADDoc.h : interface of the COpenCADDoc class
//


#pragma once
#include <DbDatabase.h>


class COpenCADDoc : public CDocument
{
protected: // create from serialization only
	COpenCADDoc();
	DECLARE_DYNCREATE(COpenCADDoc)

// Attributes
public:
    OdDbDatabasePtr m_pDatabase; // note this is a smart pointer

// Operations
public:

// Overrides
public:
    virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);

	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
#ifdef SHARED_HANDLERS
	virtual void InitializeSearchContent();
	virtual void OnDrawThumbnail(CDC& dc, LPRECT lprcBounds);
#endif // SHARED_HANDLERS

// Implementation
public:
	virtual ~COpenCADDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

#ifdef SHARED_HANDLERS
	// Helper function that sets search content for a Search Handler
	void SetSearchContent(const CString& value);
#endif // SHARED_HANDLERS
};
