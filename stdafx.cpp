
// stdafx.cpp : source file that includes just the standard includes
// OpenCAD.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

// lib
// Places a library - search record in the object file.This comment type must be accompanied 
// by a commentstring parameter containing the name(and possibly the path) of the library that
// you want the linker to search.The library name follows the default library - search records
// in the object file; the linker searches for this library just as if you had named it on the
// command line provided that the library is not specified with / nodefaultlib.
// You can place multiple library - search records in the same source file; each record appears
// in the object file in the same order in which it is encountered in the source file.

#pragma comment(lib, "TD_Alloc.lib")
// #pragma comment(lib, "TD_Alloc_4.01_14.lib")
#pragma comment(lib, "TD_Root.lib")
#pragma comment(lib, "TD_Db.lib")
#pragma comment(lib, "TD_DbRoot.lib")
#pragma comment(lib, "TD_Ge.lib")
#pragma comment(lib, "TD_Key.lib")

#include "stdafx.h"


