
// OpenCADView.h : interface of the COpenCADView class
//

#pragma once
#include <GiContextForDbDatabase.h>
#include <Gs/Gs.h>

class COpenCADView : public CView, OdGiContextForDbDatabase
{
protected: // create from serialization only
	COpenCADView();
	DECLARE_DYNCREATE(COpenCADView)

    using CView::operator new;
    using CView::operator delete;
    void addRef() {}
    void release() {}

// Attributes
public:
	COpenCADDoc* GetDocument() const;

    OdGsDevicePtr m_pDevice; // Vectorizer device
    ODCOLORREF m_clrBackground; // Drawing background color
    OdGsView::RenderMode m_iRenderMode; // Render mode

// Operations
public:

    void ResetDevice(BOOL bZoomExtents = FALSE);
    void SetViewportBorderProperties(OdGsDevice* pDevice, BOOL bModel);
    OdGsViewPtr GetView();
    void ViewZoomExtents();
    const ODCOLORREF* CurrentPalette();
    void SetRenderMode(OdGsView::RenderMode iRenderMode);

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
    virtual void OnInitialUpdate(); // called first time after construct
    void OnSize(UINT nType, int cx, int cy);
    void OnPaint();
    BOOL OnEraseBkgnd(CDC * pDC);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~COpenCADView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
    afx_msg void OnRendermode2dwireframe();
    afx_msg void OnUpdateRendermode2dwireframe(CCmdUI *pCmdUI);
    afx_msg void OnRendermode3dwireframe();
    afx_msg void OnUpdateRendermode3dwireframe(CCmdUI *pCmdUI);
    afx_msg void OnRendermode3dhidden();
    afx_msg void OnUpdateRendermode3dhidden(CCmdUI *pCmdUI);
    afx_msg void OnRendermodeFlatshaded();
    afx_msg void OnUpdateRendermodeFlatshaded(CCmdUI *pCmdUI);
    afx_msg void OnRendermodeFlatshadedwireframe();
    afx_msg void OnUpdateRendermodeFlatshadedwireframe(CCmdUI *pCmdUI);
    afx_msg void OnRendermodeSmoothshaded();
    afx_msg void OnUpdateRendermodeSmoothshaded(CCmdUI *pCmdUI);
    afx_msg void OnRendermodeSmoothshadedwireframe();
    afx_msg void OnUpdateRendermodeSmoothshadedwireframe(CCmdUI *pCmdUI);
};

#ifndef _DEBUG  // debug version in OpenCADView.cpp
inline COpenCADDoc* COpenCADView::GetDocument() const
   { return reinterpret_cast<COpenCADDoc*>(m_pDocument); }
#endif

