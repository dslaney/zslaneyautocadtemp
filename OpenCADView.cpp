
// OpenCADView.cpp : implementation of the COpenCADView class
//


#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "OpenCAD.h"
#endif

#include "OpenCADDoc.h"
#include "OpenCADView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include <DbGsManager.h>
#include <RxVariantValue.h>
#include <AbstractViewPE.h>
#include <ColorMapping.h>


// COpenCADView

IMPLEMENT_DYNCREATE(COpenCADView, CView)

BEGIN_MESSAGE_MAP(COpenCADView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
    ON_WM_SIZE()
    ON_WM_PAINT()
    ON_WM_ERASEBKGND()
    ON_COMMAND(ID_RENDERMODE_2DWIREFRAME, &COpenCADView::OnRendermode2dwireframe)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_2DWIREFRAME, &COpenCADView::OnUpdateRendermode2dwireframe)
    ON_COMMAND(ID_RENDERMODE_3DWIREFRAME, &COpenCADView::OnRendermode3dwireframe)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_3DWIREFRAME, &COpenCADView::OnUpdateRendermode3dwireframe)
    ON_COMMAND(ID_RENDERMODE_3DHIDDEN, &COpenCADView::OnRendermode3dhidden)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_3DHIDDEN, &COpenCADView::OnUpdateRendermode3dhidden)
    ON_COMMAND(ID_RENDERMODE_FLATSHADED, &COpenCADView::OnRendermodeFlatshaded)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_FLATSHADED, &COpenCADView::OnUpdateRendermodeFlatshaded)
    ON_COMMAND(ID_RENDERMODE_FLATSHADEDWIREFRAME, &COpenCADView::OnRendermodeFlatshadedwireframe)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_FLATSHADEDWIREFRAME, &COpenCADView::OnUpdateRendermodeFlatshadedwireframe)
    ON_COMMAND(ID_RENDERMODE_SMOOTHSHADED, &COpenCADView::OnRendermodeSmoothshaded)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_SMOOTHSHADED, &COpenCADView::OnUpdateRendermodeSmoothshaded)
    ON_COMMAND(ID_RENDERMODE_SMOOTHSHADEDWIREFRAME, &COpenCADView::OnRendermodeSmoothshadedwireframe)
    ON_UPDATE_COMMAND_UI(ID_RENDERMODE_SMOOTHSHADEDWIREFRAME, &COpenCADView::OnUpdateRendermodeSmoothshadedwireframe)
END_MESSAGE_MAP()

// COpenCADView construction/destruction


COpenCADView::COpenCADView()
{
	// TODO: add construction code here
    m_clrBackground = RGB(0, 0, 0);
    m_iRenderMode = OdGsView::k2DOptimized;
}

COpenCADView::~COpenCADView()
{
}


void COpenCADView::SetRenderMode(OdGsView::RenderMode iRenderMode) {
    // Get the first view
    OdGsViewPtr pView = GetView();

    // Check if render mode needs to be changed
    if (pView->mode() == iRenderMode)
        return;

    // Set the render mode
    pView->setMode(iRenderMode);

    // Check if render mode was changed
    if (pView->mode() != iRenderMode) {
        MessageBox(_T("Sorry, this render mode is not supported by the current device"), _T("OpenCAD"), MB_ICONWARNING);
    } else {
        // Show the new render mode
        PostMessage(WM_PAINT);
        m_iRenderMode = iRenderMode;
    }

}

// note - specify Vectorization model here
void COpenCADView::ResetDevice(BOOL bZoomExtents)
{
    // Get the client rectangle
    CRect rc;
    GetClientRect(&rc);

    // Load the vectorization module
    OdGsModulePtr pGs = ::odrxDynamicLinker()->loadModule("WinDirectX_4.01_14.txv"); // WinBitmap_4.01_14.txv, WinComposite_4.01_14.txv, WinGDI_4.01_14.txv, WinGLES2_4.01_14.txv, WinOpenGL_4.01_14.txv
    if (pGs.isNull()) {
        MessageBox(_T("Can't load vectorization module " OdWinOpenGLModuleName), _T("OpenCAD - Rendering aborted"), MB_ICONERROR);
        exit(1);
    }

    // Create a new OdGsDevice object, and associate with the vectorization GsDevice
    m_pDevice = pGs->createDevice();
    if (m_pDevice.isNull())
        return;

    // Return a pointer to the dictionary entity containing the device properties
    OdRxDictionaryPtr pProperties = m_pDevice->properties();

    // Set the window handle for this GsDevice
    pProperties->putAt("WindowHWND", OdRxVariantValue((long)m_hWnd));

    // Define a device coordinate rectangle equal to the client rectangle
    OdGsDCRect gsRect(rc.left, rc.right, rc.bottom, rc.top);

    // Set the device background color and palette
    m_pDevice->setBackgroundColor(m_clrBackground);
    m_pDevice->setLogicalPalette(CurrentPalette(), 256);

    if (!database())
        return;

    // Set up the views for the active layout
    m_pDevice = OdDbGsManager::setupActiveLayoutViews(m_pDevice, this);

    // Return true if and only the current layout is a paper space layout
    BOOL bModelSpace = (GetDocument()->m_pDatabase->getTILEMODE() == 0);

    // Set the viewport border properties
    SetViewportBorderProperties(m_pDevice, !bModelSpace);

    if (bZoomExtents)
        ViewZoomExtents();

    // Update the client rectangle
    OnSize(0, rc.Width(), rc.Height());

    // Redraw the window
    RedrawWindow();
}

void COpenCADView::SetViewportBorderProperties(OdGsDevice* pDevice, BOOL bModel) {
    // If current layout is Model, and it has more then one viewport then make their borders visible.
    // If current layout is Paper, then make visible the borders of all but the overall viewport.
    int n = pDevice->numViews();

    if (n > 1) {
        for (int i = bModel ? 0 : 1; i < n; ++i) {
            // Get the viewport
            OdGsViewPtr pView = pDevice->viewAt(i);

            // Make it visible
            pView->setViewportBorderVisibility(true);

            // Set the color and width
            pView->setViewportBorderProperties(CurrentPalette()[7], 1);
        }
    }
}

 
OdGsViewPtr COpenCADView::GetView()
{
  return m_pDevice->viewAt(0);
}
 
void COpenCADView::ViewZoomExtents() 
{
  // Get the overall viewport
  OdGsViewPtr pView = GetView();
 
  // Modifies the viewport to fit the extents
  OdAbstractViewPEPtr(pView)->zoomExtents(pView);
}
 
const ODCOLORREF* COpenCADView::CurrentPalette()
{
  const ODCOLORREF *pColor = odcmAcadPalette(m_clrBackground);
  return pColor;
}

void COpenCADView::OnInitialUpdate() {
    __super::OnInitialUpdate();

    // TODO: Add your specialized code here and/or call the base class

    OdGiContextForDbDatabase::setDatabase(GetDocument()->m_pDatabase); // so this is where we get the dbase from which the drawing is made
    enableGsModel(true);
    ResetDevice(true);
}

void COpenCADView::OnSize(UINT nType, int cx, int cy) {
    __super::OnSize(nType, cx, cy);

    // TODO: Add your message handler code here

    if (!m_pDevice.isNull() && cx && cy) {
        CRect rc;
        GetClientRect(rc);

        // Update the client rectangle
        OdGsDCRect Rect(OdGsDCPoint(rc.left, rc.bottom), OdGsDCPoint(rc.right, rc.top));
        m_pDevice->onSize(Rect);
    }
}

void COpenCADView::OnPaint() {
    CPaintDC dc(this); // device context for painting

                       // TODO: Add your message handler code here
                       // Do not call __super::OnPaint() for painting messages

                       // Paint the client rectangle with the GS device
    if (!m_pDevice.isNull())
        m_pDevice->update();
}

BOOL COpenCADView::OnEraseBkgnd(CDC* pDC) {
    // TODO: Add your message handler code here and/or call default
    //return __super::OnEraseBkgnd(pDC);

    return TRUE;
}



BOOL COpenCADView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// COpenCADView drawing

void COpenCADView::OnDraw(CDC* /*pDC*/)
{
	COpenCADDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// COpenCADView printing

BOOL COpenCADView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void COpenCADView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void COpenCADView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// COpenCADView diagnostics

#ifdef _DEBUG
void COpenCADView::AssertValid() const
{
	CView::AssertValid();
}

void COpenCADView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

COpenCADDoc* COpenCADView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(COpenCADDoc)));
	return (COpenCADDoc*)m_pDocument;
}
#endif //_DEBUG

// COpenCADView message handlers

void COpenCADView::OnRendermode2dwireframe() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::k2DOptimized);
}

void COpenCADView::OnUpdateRendermode2dwireframe(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::k2DOptimized);
}

void COpenCADView::OnRendermode3dwireframe() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kWireframe);
}

void COpenCADView::OnUpdateRendermode3dwireframe(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kWireframe);
}

void COpenCADView::OnRendermode3dhidden() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kHiddenLine);
}

void COpenCADView::OnUpdateRendermode3dhidden(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kHiddenLine);
}

void COpenCADView::OnRendermodeFlatshaded() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kFlatShaded);
}

void COpenCADView::OnUpdateRendermodeFlatshaded(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kFlatShaded);
}

void COpenCADView::OnRendermodeFlatshadedwireframe() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kFlatShadedWithWireframe);
}

void COpenCADView::OnUpdateRendermodeFlatshadedwireframe(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kFlatShadedWithWireframe);
}

void COpenCADView::OnRendermodeSmoothshaded() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kGouraudShaded);
}

void COpenCADView::OnUpdateRendermodeSmoothshaded(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kGouraudShaded);
}

void COpenCADView::OnRendermodeSmoothshadedwireframe() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::kGouraudShadedWithWireframe);
}

void COpenCADView::OnUpdateRendermodeSmoothshadedwireframe(CCmdUI* pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::kGouraudShadedWithWireframe);
}

/*
void COpenCADView::OnRendermode2dwireframe() {
    // TODO: Add your command handler code here
    SetRenderMode(OdGsView::k2DOptimized);
}


void COpenCADView::OnUpdateRendermode2dwireframe(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
    pCmdUI->SetCheck(m_iRenderMode == OdGsView::k2DOptimized);
}


void COpenCADView::OnRendermode3dwireframe() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermode3dwireframe(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}


void COpenCADView::OnRendermode3dhidden() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermode3dhidden(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}


void COpenCADView::OnRendermodeFlatshaded() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermodeFlatshaded(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}


void COpenCADView::OnRendermodeFlatshaded32777() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnRendermodeFlatshadedwireframe() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermodeFlatshadedwireframe(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}


void COpenCADView::OnRendermodeSmoothshaded() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermodeSmoothshaded(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}


void COpenCADView::OnRendermodeSmoothshadedwireframe() {
    // TODO: Add your command handler code here
}


void COpenCADView::OnUpdateRendermodeSmoothshadedwireframe(CCmdUI *pCmdUI) {
    // TODO: Add your command update UI handler code here
}
*/
