
// OpenCADDoc.cpp : implementation of the COpenCADDoc class
//

#include "stdafx.h"

// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "OpenCAD.h"
#endif

#include "OpenCADDoc.h"

#include <propkey.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COpenCADDoc

IMPLEMENT_DYNCREATE(COpenCADDoc, CDocument)

BEGIN_MESSAGE_MAP(COpenCADDoc, CDocument)
	ON_COMMAND(ID_FILE_SEND_MAIL, &COpenCADDoc::OnFileSendMail)
	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, &COpenCADDoc::OnUpdateFileSendMail)
END_MESSAGE_MAP()


// COpenCADDoc construction/destruction

COpenCADDoc::COpenCADDoc()
{
	// TODO: add one-time construction code here

}

COpenCADDoc::~COpenCADDoc()
{
}

// note this gets called on program start but not sure where that happens in the code ?
// or where in our project setup we specify that it should happen ?
BOOL COpenCADDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

    // Later when you read a .dwg file, its contents will be stored in the object pointed to by
    // the m_pDatabase smart pointer. However, for a new empty document, you need to create a
    // new database with the bare minimum requirements. This is done using the createDatabase()
    // method of the OdDbhostAppServices helper class. 
    // Call this method in COpenCADDoc::OnNewDocument() just before the return statement.

    try {
        m_pDatabase = theApp.createDatabase();
    } catch (OdError& err) {
        MessageBox(AfxGetMainWnd()->m_hWnd, err.description(), _T("File Read Error"), MB_ICONWARNING);
    }

	return TRUE;
}


BOOL COpenCADDoc::OnOpenDocument(LPCTSTR lpszPathName) {
    if (!CDocument::OnOpenDocument(lpszPathName)) // ask Paul why we do this ?
        return FALSE;

    // TODO: Add your specialized creation code here
    try {
        m_pDatabase = theApp.readFile(lpszPathName, true, false);
    } catch (OdError& /*err*/) {
        MessageBox(AfxGetMainWnd()->m_hWnd, _T("Unable to read file"), _T("File Read Error"), MB_ICONWARNING);
        return FALSE;
    }

   /* CString s;
    s.Format(_T("Filename: %s\nVersion: %d\n"),
        m_pDatabase->getFilename().c_str(),
        m_pDatabase->originalFileVersion());
    AfxMessageBox(s); */

    return TRUE;
}



// COpenCADDoc serialization

void COpenCADDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

#ifdef SHARED_HANDLERS

// Support for thumbnails
void COpenCADDoc::OnDrawThumbnail(CDC& dc, LPRECT lprcBounds)
{
	// Modify this code to draw the document's data
	dc.FillSolidRect(lprcBounds, RGB(255, 255, 255));

	CString strText = _T("TODO: implement thumbnail drawing here");
	LOGFONT lf;

	CFont* pDefaultGUIFont = CFont::FromHandle((HFONT) GetStockObject(DEFAULT_GUI_FONT));
	pDefaultGUIFont->GetLogFont(&lf);
	lf.lfHeight = 36;

	CFont fontDraw;
	fontDraw.CreateFontIndirect(&lf);

	CFont* pOldFont = dc.SelectObject(&fontDraw);
	dc.DrawText(strText, lprcBounds, DT_CENTER | DT_WORDBREAK);
	dc.SelectObject(pOldFont);
}

// Support for Search Handlers
void COpenCADDoc::InitializeSearchContent()
{
	CString strSearchContent;
	// Set search contents from document's data. 
	// The content parts should be separated by ";"

	// For example:  strSearchContent = _T("point;rectangle;circle;ole object;");
	SetSearchContent(strSearchContent);
}

void COpenCADDoc::SetSearchContent(const CString& value)
{
	if (value.IsEmpty())
	{
		RemoveChunk(PKEY_Search_Contents.fmtid, PKEY_Search_Contents.pid);
	}
	else
	{
		CMFCFilterChunkValueImpl *pChunk = NULL;
		ATLTRY(pChunk = new CMFCFilterChunkValueImpl);
		if (pChunk != NULL)
		{
			pChunk->SetTextValue(PKEY_Search_Contents, value, CHUNK_TEXT);
			SetChunkValue(pChunk);
		}
	}
}

#endif // SHARED_HANDLERS

// COpenCADDoc diagnostics

#ifdef _DEBUG
void COpenCADDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void COpenCADDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// COpenCADDoc commands
